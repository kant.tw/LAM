$(document).ready(function() {
	$('.mainNav > ul > li > a').on('click', function(e) {
		e.preventDefault();
	});
	$('.mainNav > ul > li > a').mouseenter(function(e) {
		// console.log('mouseenter: ' + $(this).next('.submenu').length);
		$('.submenu').removeClass('active');
		$(this).next('.submenu').addClass('active');
	});
	$('.mainNav > ul').mouseleave(function(e) {
		// console.log('mouseleave');
		$('.submenu').removeClass('active');
	});

	$('.js-hoverchangetxt').hover(function(e) { //hover in
		var engTxt = $(e.target).data('eng');
		$(e.target).text(engTxt);
	}, function(e) {
		var chineseTxt = $(e.target).data('ch');
		$(e.target).text(chineseTxt);
	});

	$('.socialInfo .contact').on('click', function() {
		$(this).next('.contactContent').toggle();
	});

	$('.homeMainBanner').slick({
		dots: true,
		autoplay: true,
		autoplaySpeed: 3000,
		pauseOnFocus: false,
		pauseOnHover: false
	});

	//首頁點look more
	var currentIdx = 0;
	var oneceLoad = 8;
	$('.lookmore').on('click', function(e) {
		e.preventDefault();
		
		$('.bestItemWrap .hiddenPd').each(function(idx) {
			console.log('test:' + idx);
			if (idx < oneceLoad) {
				$(this).removeClass('hiddenPd');
			}
		});
		if ($('.bestItemWrap .hiddenPd').length === 0) {
			$(this).css('display', 'none');
		}
	});

	//header toggle
	$('#toggleHeader').on('click', function(e) {
		e.preventDefault();
		$('body').toggleClass('headerStickTop');
	});

	//切換兩欄或四欄
	$('#layoutSwitch a').on('click', function(e) {
		e.preventDefault();
		var layoutSwitchTarget = $(this).parent('#layoutSwitch').data('controltarget');
		if ($(this).data('layout') === 'col2') {
			$('#' + layoutSwitchTarget).removeClass('fourCol').addClass('twoCol');
		} else { //如果不是點2欄就顯示4欄
			$('#' + layoutSwitchTarget).removeClass('twoCol').addClass('fourCol');
		}
	});

	//select box it
	$('select').selectBoxIt({autoWidth: false});
	

	//pdContent show correct size and number~
	if (('.detailbox.size').length > 0) {
		switchSizeNumber();
	}
	$('.activeBtnsWrap a').hover(function(e) {
		$(e.target).addClass('hover');
	}, function(e) {
		$(e.target).removeClass('hover');
	});

	var pdContentSize, pdContentColor, pdContentQty, pdContentFinal, pdContentMax;
	function switchSizeNumber() {
		var activeColor = $('.colorSelector').find('.active').data('color');
		if (activeColor === undefined) { 
			activeColor = $('.colorSelector a').eq(0).data('color'); 
			$('.colorSelector a').eq(0).addClass('active');
		}
		pdContentColor = activeColor;
		$('.detailbox.size').find('.detailValue').each(function() {
		    if (activeColor === $(this).data('color')) {
			    $(this).show();

			    $(this).find('select').val($(this).find('option').eq(0).text());
			    pdContentSize = $(this).find('option').eq(0).text();
			    var targetQty = $(this).find('option').eq(0).data('qty');
			    $(this).find('select').val(targetQty);
			    pdContentQty = 1;
			    constructQty(targetQty);
			    pdContentMax = $(this).find('option').eq(0).data('max');

			    constructFinalValue(pdContentColor, pdContentSize, pdContentQty, pdContentMax);
			} else {
			    $(this).hide();
			    
			}
		});
	}

	
	//pdContent color change
	$('.colorSelector a').on('click', function(e) {
		e.preventDefault();
		if (!$(this).hasClass('active')) {
			$('.colorSelector a').removeClass('active');
			$(this).addClass('active');
			pdContentColor = $(this).data('color');
		}
		switchSizeNumber();
	});

	$('.detailbox.size select').on('change', function(e) {
		pdContentSize = $(this).val();
		pdContentQty = 1;
		var currentQty = parseInt($(this).find('option:selected').data('qty'), 10);
		
		pdContentMax = $(this).find('option:selected').data('max');
		constructFinalValue(pdContentColor, pdContentSize, pdContentQty, pdContentMax);
		constructQty(currentQty);
	});

	$('.detailbox.number select').on('change', function(e) {
		pdContentQty = $(this).val();
		constructFinalValue(pdContentColor, pdContentSize, pdContentQty, pdContentMax);
	});

	function constructQty(n) {
		var qtyHtml = '';
		var qtySelectBoxItObj = $('.detailbox.number').eq(0).find('select').data('selectBox-selectBoxIt');
		for (var i = 1; i <= n; i++) {
			qtyHtml = qtyHtml + '<option value="' + i + '">' + i + '</option>';
		}
		
		qtySelectBoxItObj.destroy();
		$('.detailbox.number').eq(0).find('select').empty().append(qtyHtml);
		$('.detailbox.number').eq(0).find('select').selectBoxIt();
	}

	function constructFinalValue(color, size, number, maxnumber) {
		pdContentFinal = 'final:' + color + '::' + size + '::' + number + '::' + maxnumber;
		console.log('value: ' + pdContentFinal);
		var pdContentFinalInput = document.formPD.selectpd; //要塞值進去的地方
		pdContentFinalInput.value = pdContentFinal;
	}

	//pdContent you may also like slider
	$('.alsoLikeSlider').slick({
		slidesToShow: 5,
		slidesToScroll: 1,
		arrows: true
	});

	//pdContent lightbox
	// $('.detailBaseTrigger').magnificPopup({
	// 	callbacks: {
	// 		open: function() {
	// 			for (var key in this) {
	// 				console.log(key);
	// 			}
	// 			bindSizeRuleTab();
	// 		}
	// 	}
	// });
	$('.detailBaseTrigger').on('click', function(e) {
		e.preventDefault();
		var tabindex = $(this).data('tab');
		$.magnificPopup.open({
			items: {
				src: '#sizeMeasureModel'
			},
			type: 'inline',
			callbacks: {
				elementParse: function(item) {
					console.log('test: ' + tabindex);
					bindSizeRuleTab(tabindex);
				}
			}
		});
	});


	//付款方式
	$('.paywaySelector').on('change', function(e) {
		var targetParagraph = '#' + $(this).val() + 'Intro';
		var contentWrap = $(this).parents('.paywayWrap').find('.payway-right');
		var payWayIndex = $(this).parents('.paywayWrap').data('payway');
		contentWrap.find('p').removeClass('active');
		contentWrap.find(targetParagraph).addClass('active');
		if (payWayIndex === '1') {
			var defaultVal = $(this).parents('.paywayWrap').next('.paywayWrap').find('.paywaySelector').find('option').eq(0).val();
			$(this).parents('.paywayWrap').next('.paywayWrap').find('.paywaySelector').val(defaultVal);
			$(this).parents('.paywayWrap').next('.paywayWrap').find('.payway-right p').removeClass('active');
		}
	});

	// //會員中心 修改按鈕
	// $('.toggleHiddenInput').on('click', function(e) {
	// 	e.preventDefault();
	// 	var parentbox = $(this).parents('.inputbox');
	// 	if (parentbox.hasClass('opened')) {
	// 		$(this).text('修改');
	// 	} else {
	// 		$(this).text('取消修改');
	// 	}
	// 	parentbox.toggleClass('opened');
	// });

	//goTop
	$('#goTop a').on('click', function(e) {
		e.preventDefault();
		$('body, html').animate({
			scrollTop: 0
		}, 500);
	});

	//orderdata invoice type change
	$('input[name="invoice"]').on('change', function() {
		// console.log('change ' + $(this).val());
		var activeInvoiceType = $(this).val();
		$('.invoiceType').each(function() {
			if ($(this).attr('id') === activeInvoiceType) {
				$(this).addClass('active');
			} else {
				$(this).removeClass('active');
				$(this).find('.deviceBarcode').hide();
			}
		});
	});
	$('#showBarcodeInput').on('click', function(e) {
		e.preventDefault();
		$(this).parents('.invoiceType').find('.deviceBarcode').show();
	});

	//確認必填欄位
	// $('.js-checkInputFilled').on('click', function(e) {
	// 	e.preventDefault();
	// 	var targetForm = $(this).data('checkform');
	// 	var tocheckInputs = $(document[targetForm]).find('.requiredInput');
	// 	var formOk = true;
	// 	$.each(tocheckInputs, function(idx, inputElem) {
	// 		var errMessage = $(inputElem).data('errmsg');
			
	// 		if (((inputElem.tagName).toUpperCase() === 'INPUT') && ($(inputElem).val() === '')) {
	// 			alert('請填寫' + errMessage);
	// 			formOk = false;
	// 			return false;
	// 		}
	// 		if (((inputElem.tagName).toUpperCase() === 'SELECT') && ($(inputElem).val() === '')) {
	// 			alert('請選擇' + errMessage);
	// 			formOk = false;
	// 			return false;
	// 		}
	// 	});

	// 	if (targetForm === 'deliverform') {
	// 		//驗證發票
	// 		// console.log('invoice ' + document[targetForm].invoice.value);
	// 		checkRequiredRadio(document[targetForm].invoice);
	// 	}

	// 	if (formOk) {
	// 		if (targetForm === 'loginForm') {
	// 			cmdLogin();
	// 		} else (targetForm === 'registForm') {
	// 			document.registForm.submit();
	// 		}
	// 		console.log('go');	
	// 	}

	// });

	function bindSizeRuleTab(activeIdx) {
		if (activeIdx === undefined) {
			activeIdx = 0;
		}

		$('#sizeMeasureModel').find('.pddetailTabsContent').removeClass('active');
		$('#sizeMeasureModel').find('.pddetailTabsContent').hide();
		$('#sizeMeasureModel').find('.pddetailTabs a').removeClass('active');

		// pdContent tab
		$('#sizeMeasureModel').find('.pddetailTabsContent').eq(activeIdx).show();
		$('#sizeMeasureModel').find('.pddetailTabs li').eq(activeIdx).find('a').addClass('active');

		$('.pddetailTabs a').on('click', function(e) {
			e.preventDefault();
			var targetDiv = $(this).attr('href');
			var allCntDiv = $(this).parents('#sizeMeasureModel').find('.pddetailTabsContent');

			$(this).parents('.pddetailTabs').find('a').removeClass('active');
			$(this).addClass('active');
			allCntDiv.each(function(index) {
				if ($(this).attr('id') !== targetDiv.substring(1)) {
					$(this).fadeOut();
				} else {
					$(this).fadeIn();
				}
			});

		});
	}


	function checkRequiredRadio(radioInput) {
		if (radioInput.value === '') {
			alert('請選擇發票');
			return false;
		} else {
			switch (radioInput.value) {
				case 'invoiceType3':
					if (($('.companyId').val() === '') || ($('.companyTitle').val() === '')) {
						alert('請填公司統編及抬頭');
						return false;
					}
					break;
				default: 
				break;
			}
		}
	}

});