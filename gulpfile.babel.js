import gulp from 'gulp';

import sass from 'gulp-ruby-sass';

import browserSync from 'browser-sync';

import del from 'del';

import fileinclude from 'gulp-file-include';

import sourcemaps from 'gulp-sourcemaps';

gulp.task('fileinclude', () => {
	runFileinclude();
});

function runFileinclude() {
	gulp.src(['rawHtml/*.html'])
	    .pipe(fileinclude({
	    	prefix: '@@',
	    	basepath: '@file',
	    	indent: true
	    }))
	    .pipe(gulp.dest('./'));
	console.log('finish fileinclude');
}

gulp.task('sass', () =>
	sass('sass/main.scss', {sourcemap: true})
	.on('error', sass.logError)
	.pipe(sourcemaps.write())
	// .pipe(sourcemaps.write('maps', {
	// 	includeContent: false,
	// 	sourceRoot: 'source'
	// }))
	.pipe(gulp.dest('css'))
	.pipe(browserSync.stream({match: '**/*.css'}))
);

gulp.task('serve', ['sass', 'fileinclude'], () => {
	browserSync.init({
		server: {
			baseDir: './'
		}
	});

	gulp.watch('sass/**/*', ['sass']);
	gulp.watch('rawHtml/**/*.html').on('change', function() {
		console.log('rawHtml template change');
    	runFileinclude();
    });
    gulp.watch('rawHtml/*.html').on('change', function() {
		console.log('rawHtml firstlevel change');
    	runFileinclude();
    });
	gulp.watch('*.html').on('change', browserSync.reload);
	gulp.watch('js/main.js').on('change', browserSync.reload);
});

gulp.task('cleanDist', () => {
	return del([
		'dist/**/*'
	]);
});

gulp.task('build', ['sass', 'cleanDist'], () => {
	gulp.src('*.html').pipe(gulp.dest('./dist/'));
	gulp.src('./css/*.css').pipe(gulp.dest('./dist/css/'));
	gulp.src('./js/**/*').pipe(gulp.dest('./dist/js/'));
	gulp.src('./img/**/*').pipe(gulp.dest('./dist/img/'));
	gulp.src('./fonts/*').pipe(gulp.dest('./dist/fonts/'));
	// gulp.src('./vendors/bootstrap-sass-master/assets/**/**/*').pipe(gulp.dest('./dist/vendors/bootstrap-sass-master/assets/'));
	// gulp.src('./vendors/font-awesome-4.4.0/**/*').pipe(gulp.dest('./dist/vendors/font-awesome-4.4.0/'));
});

gulp.task('default', ['serve']);